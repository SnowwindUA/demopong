﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoresController : MonoBehaviour {

    public GameController gameController;

    public bool isPlayerBase;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collision!");

        if (collision.collider.CompareTag("ball"))
        {

            isPlayerBase = CompareTag("player_base");

            if (isPlayerBase)
            {
                Debug.Log("AI GET SCORE");
                gameController.AIGetScore();
            }
            else
            {
                gameController.PlayerGetScore();
            }
        }
    }
}
