﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPadControl : MonoBehaviour {

    public float speed = 10.0f;
    private Vector2 movePosition;

	// Use this for initialization
	void Start () {

        movePosition = new Vector2(gameObject.GetComponent<Rigidbody2D>().position.x, 0);

    }

    public void CheckMove()
    {
        Vector2 velocity = gameObject.GetComponent<Rigidbody2D>().velocity;

        if (Input.GetMouseButton(0))
        {
            Vector2 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Move(velocity, clickPosition);
        }
        else if(Input.touchCount > 0)
        {
            Vector2 touchPosition = Input.GetTouch(0).position;
            Move(velocity, touchPosition);
        }
        else
        {
            Stay(velocity);
        }
    }



    private void Move(Vector2 newVelocity, Vector2 clickPosition)
    {

        newVelocity.y = CheckSpeedDirection(clickPosition);
        GetComponent<Rigidbody2D>().velocity = newVelocity;
    }

    private float CheckSpeedDirection(Vector2 clickPosition)
    {
        float difference = transform.position.y - clickPosition.y;
        if (difference > 0.1f || difference < -0.1f)
        {
            if (clickPosition.y > transform.position.y)
            {
                return speed;
            }
            else
            {
                return -speed;
            }
        }
        else
        {
            return 0;
        }
    }



    private void Stay(Vector2 newVelocity)
    {
        newVelocity.y = 0;
        GetComponent<Rigidbody2D>().velocity = newVelocity;
    }
}
