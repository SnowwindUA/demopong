﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    public float speed = 15.0f;
    private Vector2 movePosition;
    public Transform ball;

    // Use this for initialization
    void Start()
    {

        movePosition = new Vector2(gameObject.GetComponent<Rigidbody2D>().position.x, 0);

    }

    public void Move()
    {
        Vector2 newVelocity = GetComponent<Rigidbody2D>().velocity;

        Vector2 ballPosition = ball.position;

        newVelocity.y = CheckSpeedDirection(ballPosition);
        GetComponent<Rigidbody2D>().velocity = newVelocity;
    }

    private float CheckSpeedDirection(Vector2 ballPosition)
    {
        float difference = transform.position.y - ballPosition.y;
        if (difference > 0.1f || difference < -0.1f)
        {
            if (ballPosition.y > transform.position.y)
            {
                return speed;
            }
            else
            {
                return -speed;
            }
        }
        else
        {
            return 0;
        }
        
    }
}
