﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public int PlayerScore;
    public int AIScore;

    public BallController ballController;
    public PlayerPadControl playerPadControl;
    public AIController aiController;

    public GUIStyle guiStyle;

    private void OnGUI()
    {
        GUI.Label(new Rect(Screen.width / 4, 50, 100, 25), PlayerScore.ToString(), guiStyle);
        GUI.Label(new Rect(Screen.width - Screen.width / 4, 50, 100, 25), AIScore.ToString(), guiStyle);
    }
   
	void FixedUpdate () {
        ballController.PreventStuck();
        ballController.SpeedLimit();
        playerPadControl.CheckMove();
        aiController.Move();
	}

    public void AIGetScore()
    {
        AIScore++;
        NewRound();
    }

    public void PlayerGetScore()
    {
        PlayerScore++;
        NewRound();
    }

    private void NewRound()
    {
        ballController.ResetBall();
    }

}
