﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    public int StartSpeed = 250;
    public float HorizontalBounceFactor = 1.1f;
    public int velocityLimit = 12;

    private int punchDirectionFactor;

    private float stuckTimer;

	// Use this for initialization
	void Start () {
        PunchBall();
	}

    public void SpeedLimit()
    {
        Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
        if(velocity.x > velocityLimit)
        {
            velocity.x = velocityLimit;
        }
        if (velocity.y > velocityLimit)
        {
            velocity.y = velocityLimit;
        }
        GetComponent<Rigidbody2D>().velocity = velocity;
    }

    public void ResetBall()
    {
        Vector2 zeroPosition = new Vector2(0, 0);
        GetComponent<Rigidbody2D>().velocity = zeroPosition;
        GetComponent<Rigidbody2D>().position = zeroPosition;

        PunchBall();
    }

    public void PreventStuck()
    {
        stuckTimer += Time.fixedDeltaTime;
        if (stuckTimer > 7)
        {
            ResetBall();
            stuckTimer = 0;
        }
    }


    private void PunchBall()
    {
        punchDirectionFactor = Random.Range(0, 9);
        int verticalFactor = Random.Range(-20, 20);

        Vector2 punch = new Vector2(PunchDirectionCheck(punchDirectionFactor), verticalFactor);

        GetComponent<Rigidbody2D>().AddForce(punch);
    }
    private int PunchDirectionCheck(int factor)
    {
        if(factor > 5)
        {
            return StartSpeed;
        }
        else
        {
            return -StartSpeed;
        }
    }

    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.collider.CompareTag("pad"))
        {
            HandleHitPad(collision);
            GetComponent<Animator>().SetTrigger("Hit");
            GetComponent<AudioSource>().Play();
        }
        else
        {
            collision.collider.GetComponent<AudioSource>().Play();
        }

    }

    private void HandleHitPad(Collision2D collision)
    {
        stuckTimer = 0;

        Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
        float yPadVelocity = collision.collider.GetComponent<Rigidbody2D>().velocity.y;

        velocity.y = (velocity.y / 2) * (yPadVelocity);

        velocity.x = velocity.x * HorizontalBounceFactor;

        GetComponent<Rigidbody2D>().velocity = velocity;
    }

}
