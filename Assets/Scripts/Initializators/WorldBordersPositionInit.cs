﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBordersPositionInit : MonoBehaviour {

    public BoxCollider2D TopWall;
    public BoxCollider2D BotWall;
    public BoxCollider2D LeftWall;
    public BoxCollider2D RightWall;

    // Use this for initialization
    void Start () {

        float xScreenRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x;
        float xScreenLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;

        float yScreenTop = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;
        float yScreenBottom = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;

        TopWall.size = new Vector2(xScreenRight*2, 0.1f);
        TopWall.offset = new Vector2(0, yScreenTop);

        BotWall.size = new Vector2(xScreenRight*2, 0.1f);
        BotWall.offset = new Vector2(0, yScreenBottom);

        LeftWall.size = new Vector2(0.1f, yScreenTop*2);
        LeftWall.offset = new Vector2(xScreenLeft, 0);

        RightWall.size = new Vector2(0.1f, yScreenTop * 2);
        RightWall.offset = new Vector2(xScreenRight, 0);
    }
}
