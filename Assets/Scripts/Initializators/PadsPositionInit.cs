﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadsPositionInit : MonoBehaviour {

    public Transform PlayerPad;
    public Transform AIPad;
	// Use this for initialization
	void Start () {

        SetPlayerPosition();
        SetAIPosition();
        
    }


    private void SetPlayerPosition()
    {
        float xPlayer = Camera.main.ScreenToWorldPoint(new Vector3(30, 0, 0)).x;
        Vector3 playerPos = PlayerPad.position;
        playerPos.x = xPlayer;
        PlayerPad.position = playerPos;
    }

    private void SetAIPosition()
    {
        float xAI = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - 30, 0, 0)).x;
        Vector3 aiPos = AIPad.position;
        aiPos.x = xAI;
        AIPad.position = aiPos;
    }
}
